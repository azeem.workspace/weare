import { Inject, Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { UtilService } from './util.service';
import { BehaviorSubject, Observable } from 'rxjs';

declare var cordova: any;

@Injectable({
  providedIn: 'root',
})
export class FireStoreService {
  publicList = new BehaviorSubject<any>([]);

  constructor(
    private utilService: UtilService,
    private firestore: AngularFirestore
  ) {}

  addRecord(record: any) {
    if (!record?.child) record.child = [];
    if (!record.collection) record.collection = [];
    record.created = new Date().toISOString();

    this.firestore
      .collection('/Records/')
      .add(record)
      .then(() => {
        this.utilService.presentToast('top', 'Successfull', 'primary');
        if (record.child && record.child.length > 0) {
          this.admissionChild(record);
        }
      });
  }

  async updateRecord(id: any, recorUpdate: any) {
    this.firestore
      .doc('Records/' + id)
      .update(recorUpdate)
      .then(() => {
        console.log('Successfully update');
        this.utilService.presentToast('top', 'Successfull', 'primary');
      });
  }

  deleteRecord(id: any) {
    // this.firestore.doc('/Records/'+id).delete()
  }

  getRecords() {
    return this.firestore.collection('/Records/').snapshotChanges();

    //////////////////////////////////////
    // this.firestore.collection('/Records/').snapshotChanges().subscribe(res=>{
    //   if(res){
    //     this.publicList = res.map((e:any)=>{
    //       return{
    //         id: e.payload.doc.id,
    //         name: e.payload.doc.data()['name'],
    //         at: e.payload.doc.data()['at'],
    //         adhar: e.payload.doc.data()['adhar'],
    //         child: e.payload.doc.data()['child'],
    //       }
    //     });

    //   }
    // });
  }

  admissionChild(record: any) {
    let child: any = {};

    for (let num of record.child) {
      child.name = num;
      child.father = record.name;
      child.adhar = record.adhar;
      child.exam = [];
      this.firestore
        .collection('/Students/')
        .add(child)
        .then(() => {
          child = {};
        });
    }
  }

  async updateChildEntry(id: any, recorUpdate: any, record: any) {
    this.firestore
      .doc('Records/' + id)
      .update({
        child: recorUpdate,
      })
      .then(() => {
        this.firestore
          .collection('/Students/')
          .snapshotChanges()
          .subscribe((res) => {
            this.utilService.presentToast('top', 'Successfull', 'primary');
            if (res) {
              let temp = res.map((e: any) => {
                return e.payload.doc.data()['name'];
              });

              for (let num of temp) {
                const index = recorUpdate.indexOf(num);
                if (index > -1) {
                  recorUpdate.splice(index, 1);
                }
              }
              record.child = recorUpdate;
              this.admissionChild(record);
            }
          });
      });
  }

  getSingleStudent(name: string) {
    return this.firestore
      .collection('Students', (ref) => ref.where('name', '==', name))
      .snapshotChanges();
  }

  getSingleCollection(name: string) {
    return this.firestore
      .collection('Records', (ref) => ref.where('adhar', '==', name))
      .snapshotChanges();
  }

  addInvoice(invoiceData: any) {
    // this.firestore
    //   .collection('/Invoices/')
    //   .add(invoiceData)
    //   .then(() => {
    //     this.utilService.presentToast('top', 'Successfull Invoices', 'primary');
    //   });

    this.firestore
      .collection('/Invoices/')
      .add(invoiceData)
      .then(() => {
        this.utilService.presentToast('top', 'Successfull Invoices', 'primary');
      });
  }

  getInvoices() {
    return this.firestore.collection('/Invoices/').snapshotChanges();
  }

  scheduleExam(record: any) {
    this.firestore
      .collection('/Exam/')
      .add(record)
      .then(() => {
        this.utilService.presentToast('top', 'Successfull', 'primary');
      });
  }

  getStudents() {
    return this.firestore.collection('/Students/').snapshotChanges();
  }

  getExams() {
    return this.firestore.collection('/Exam/').snapshotChanges();
  }

  upDateStudentExam(id: any, recorUpdate: any, record: any) {
    this.utilService.showLoading();
    this.firestore
      .doc('Students/' + id)
      .update({
        exam: recorUpdate,
      })
      .then(() => {
        this.utilService.presentToast('top', 'Successfull', 'primary');
      });
  }

  getInvoice(inNo: number) {
    return this.firestore
      .collection('Invoices', (ref) => ref.where('invoiceNo', '==', inNo))
      .snapshotChanges();
  }

  downloadPDF(template: string, fName: string) {
    cordova.plugins.pdf.htmlToPDF({
      data: template,
      documentSize: 'A4',
      landscape: 'portrait',
      type: 'share',
      fileName: fName,
    });
  }

  testing = async () => {
    let temp: any;
    this.firestore
      .collection('/Records/')
      .snapshotChanges()
      .subscribe(async (res) => {
        res.map(async (e: any) => {
          return {
            id: e.payload.doc.id,
            RegisterNo: e.payload.doc.data()['RegisterNo'],
            adhar: e.payload.doc.data()['adhar'],
            at: e.payload.doc.data()['at'],
            child: e.payload.doc.data()['child'],
            collection: e.payload.doc.data()['collection'],
            created: e.payload.doc.data()['created'],
            name: e.payload.doc.data()['name'],
          };
        });
      });
    // return await temp;
  };

  addMarriageDateRecord(record: any) {
    record.created = new Date().toISOString();

    this.firestore
      .collection('/MarriageDateRecord/')
      .add(record)
      .then(() => {
        this.utilService.presentToast('top', 'Successfull', 'primary');
      });
  }

  getMarriageDateRecord() {
    return this.firestore.collection('/MarriageDateRecord/').snapshotChanges();
  }
}
