import { Injectable } from '@angular/core';
import {
  ToastController,
  LoadingController,
  ModalController,
  AlertController,
} from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class UtilService {
  constructor(
    public modalCtrl: ModalController,
    public alertController: AlertController,
    private loadingCtrl: LoadingController,
    private toastController: ToastController
  ) {}

  async presentToast(
    position: 'top' | 'middle' | 'bottom',
    msg: string = 'Give Some Text!',
    color = 'danger'
  ) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      position: position,
      color: color,
    });

    await toast.present();
  }

  async showLoading() {
    const loading = await this.loadingCtrl.create({
      message: 'Please wait...',
      duration: 3000,
    });

    loading.present();

    setTimeout(async () => {
      let ale = this.alertController.getTop();
      (await ale) ? this.alertController.dismiss() : false;
      let md = this.modalCtrl.getTop();
      (await md) ? this.modalCtrl.dismiss() : false;
    }, 3000);
  }
}
