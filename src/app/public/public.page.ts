import { Component, OnInit } from '@angular/core';
import { environment } from './../../environments/environment';

@Component({
  selector: 'app-public',
  templateUrl: './public.page.html',
  styleUrls: ['./public.page.scss'],
})
export class PublicPage implements OnInit {
  slideOpts = {
    initialSlide: 1,
    speed: 400,
  };

  constructor() {}

  ngOnInit() {}

  myMap() {
    let _map: any = document.getElementById('googleMap');
    var mapProp = {
      center: new google.maps.LatLng(51.508742, -0.12085),
      zoom: 5,
    };
    var map = new google.maps.Map(_map, mapProp);
  }
}
