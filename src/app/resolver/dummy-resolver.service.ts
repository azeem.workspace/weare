import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { FireStoreService } from '../services/fire-store.service';
import { NamazService } from '../services/namaz.service';

@Injectable({
  providedIn: 'root',
})
export class DummyResolverService implements Resolve<any> {
  constructor(
    public fireService: FireStoreService,
    public namazTime: NamazService
  ) {}

  resolve(route: ActivatedRouteSnapshot) {
    console.log('-=-=-=', route.routeConfig?.path);
    if (route.routeConfig?.path === 'dashboard') {
      return this.namazTime.getNamaList();
    } else {
      this.fireService.testing().then((res) => {
        console.log('@@', res);
      });
      return 'home';
    }
  }
}
