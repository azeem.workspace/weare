import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NamazService } from '../services/namaz.service';
import {
  AlertController,
  LoadingController,
  ToastController,
} from '@ionic/angular';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { FireStoreService } from '../services/fire-store.service';
import { UtilService } from '../services/util.service';
import { Plugins } from '@capacitor/core';
import { IonicSlides } from '@ionic/angular';

const { LocalNotifications } = Plugins;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  details: any;
  detailsList: any;
  timeoutID: any;
  currentStamp = [];
  date: any;

  swiperModules = [IonicSlides];
  @ViewChild('swiper')
  swiperRef: ElementRef | undefined;
  marriegeList: Array<any> = [];

  constructor(
    private utilService: UtilService,
    public getNList: NamazService,
    public route: Router,
    public activeSnap: ActivatedRoute,
    private alertController: AlertController,
    public fireService: FireStoreService,
    private loadingCtrl: LoadingController,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    this.details = this.activeSnap.snapshot.data['namaz'];
    console.log('*******', this.details);
    this.detailsList = Object.entries(this.details?.data?.timings);
  }

  ionViewWillEnter() {
    this.delayTimer();
    this.getDateList();
  }

  delayTimer() {
    let date = new Date();
    this.date = date;
    // this.currentStamp = this.getTime();
    let toDate: any =
      ('0' + date.getHours()).slice(-2) +
      '-' +
      ('0' + (date.getSeconds() + 1)).slice(-2);

    toDate = toDate.substring(0, 2);
    let curr = this.detailsList[0][1].substring(0, 2);
    let diff = Math.abs(toDate - curr);

    for (let i = 0; i < this.detailsList.length; i++) {
      let dum = parseInt(this.detailsList[i][1].substring(0, 2));
      let newdiff = Math.abs(toDate - dum);
      let sAd;

      if (newdiff < diff) {
        diff = newdiff;
        this.currentStamp = this.detailsList[i];
        // this.adhanAlarm(this.detailsList[i][0]);
        // console.log('A');
      }
    }
    // console.log(this.currentStamp);
    // console.log(toDate);
    // this.currentStamp = this.detailsList.reduce(
    //   async (prev: any, curr: any) => {
    //     curr = await curr[1].substring(0, 2);
    //     prev = await prev[1].substring(0, 2);
    //     console.log(prev);
    //     return Math.abs(curr - toDate) < Math.abs(prev - toDate) ? curr : prev;
    //   });

    // console.log(toDate);

    this.timeoutID = setInterval(() => {
      this.delayTimer();
    }, 1000);
  }

  getTime() {
    // const msSinceEpoch = new Date().getTime();
    // return msSinceEpoch;

    const localTimeString = new Date().toLocaleTimeString('en-us', {
      hour: '2-digit',
      minute: '2-digit',
    });
    return localTimeString;
  }

  ionViewWillLeave() {
    clearInterval(this.timeoutID);
  }

  tConvert(time: any) {
    // Check correct time format and split into components
    if (!time) return '00:00';

    time = time
      .toString()
      .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) {
      // If time format correct

      time = time.slice(1); // Remove full string match value
      time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
      time[0] = '0' + time[0];
      time[0] = time[0].slice(-2);
    }
    return time.join(''); // return adjusted time or original string
  }

  async loginValidation(user: string) {
    const userChk = await this.alertController.create({
      message: `Please enter ${
        user == 'admin' ? 'Admin password' : 'User Aadhar'
      }.`,
      inputs: [
        {
          name: 'user',
          placeholder: `Enter ${
            user == 'admin' ? 'Admin password' : 'User name'
          }`,
          type: user == 'admin' ? 'password' : 'number',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (data) => {
            console.log('Cancel clicked.');
            // this.checkUser();
          },
        },
        {
          text: 'Okay',
          handler: (data) => {
            if (!data.user) {
              this.utilService.presentToast(
                'top',
                'Something wrong...!',
                'danger'
              );
              return;
            }
            this.utilService.showLoading();
            if (user === 'public') {
              localStorage.setItem('user', data.user);
            } else if (user === 'admin') {
              localStorage.setItem('user', user);
              // if (data.user === 'Ismail') this.route.navigate(['home']);
            }
            this.route.navigate(['home']);
            // let navigationExtras: NavigationExtras = {
            //   queryParams: {
            //     id: user,
            //   },
            //   // state: {
            //   //   user: data.user,
            //   // },
            // };
            // this.route.navigate(['home'], navigationExtras);
          },
        },
      ],
    });

    this.clearUtilServices();
    userChk.present();
  }

  adhanAlarm(title: string = 'Azan') {
    LocalNotifications['schedule']({
      notifications: [
        {
          title: title,
          body: 'Body',
          id: 1,
          schedule: { at: new Date(Date.now() + 1000 * 5) },
          sound: '../assets/audio/azan.wav',
          // attachments: null,
          // actionTypeId: "",
          // extra: null
        },
      ],
    });
  }

  clearUtilServices() {
    this.loadingCtrl.getTop().then((val) => {
      if (val) {
        this.loadingCtrl.dismiss();
      }
    });

    this.toastController.getTop().then((val) => {
      if (val) {
        this.toastController.dismiss();
      }
    });
  }

  getDateList() {
    this.fireService.getMarriageDateRecord().subscribe((result: any) => {
      if (result.length > 0) {
        this.marriegeList = result.map((e: any) => {
          return {
            id: e.payload.doc.id,
            created: e.payload.doc.data()['created'],
            endDate: e.payload.doc.data()['endDate'],
            location: e.payload.doc.data()['location'],
            message: e.payload.doc.data()['message'],
            startDate: e.payload.doc.data()['startDate'],
            title: e.payload.doc.data()['title'],
          };
        });
      }
    });

    setTimeout(() => {
      console.log('====================================');
      console.log(this.marriegeList);
      console.log('====================================');
    }, 3000);
  }
}
