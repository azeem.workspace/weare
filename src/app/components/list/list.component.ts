import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent  implements OnInit {

  @Input() dataList!:any;
  
  constructor() {
    setTimeout(()=>{
      console.log('====================================');
    console.log(this.dataList);
    console.log('====================================');
    },3000);
   }

  ngOnInit() {
   
  }


}
