// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAzy4JGhYYO26ZFxPjLNI70AM2e-W4yrAQ',
    authDomain: 'weare-cd54b.firebaseapp.com',
    projectId: 'weare-cd54b',
    storageBucket: 'weare-cd54b.appspot.com',
    messagingSenderId: '1005149484835',
    appId: '1:1005149484835:web:6599da8faf9adf1b8a3e96',
    measurementId: 'G-BRX9S8V2ER',
  },
  PRAYER_TIMING: 'http://api.aladhan.com/v1/timings/',
  GOOGLEMAP: 'https://maps.googleapis.com/maps/api/js',
  IQAIR_KEY: 'e06b78fa-0a7a-42a5-98b5-f1110041c8cc',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
